clear all
close all

Nt = 40
Nf = 40

var_n = 0.2
caf_surface_n = var_n*abs(randn(Nt, Nf));

caf_surface = caf_surface_n

peaks(1).ti = 20
peaks(1).fi = 20
peaks(1).a = 4

for peak = peaks
  caf_surface(peak.ti, peak.fi) = peak.a;
endfor
caf_surface  = interp2(interp2(caf_surface));

h = surf(caf_surface)
%h = surf(max(10*log10(caf_surface), -20))
shading interp
colormap jet
grid off
view([-58, 20])
zlim([0 20])
%caxis([0, 20])
axis off
